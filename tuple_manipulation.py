def iter_pixels(d, n):
    pixel = [0] * d
    yield tuple(pixel)
    while pixel != [n-1] * d:
        i = 0
        while pixel[i] == n-1:
            pixel[i] = 0
            i += 1
        pixel[i] += 1
        yield tuple(pixel)


def add_pixels(p0, p1):
    if len(p0) != len(p1):
        raise ValueError
    return tuple(p0[i] + p1[i] for i in range(len(p0)))


def insert_in_tuple(tple, index, value):
    return tple[:index] + (value,) + tple[index:]


def multiply_tuple(tple, scalar):
    return tuple(value * scalar for value in tple)


def tuple_index_mod(tple, index, mod):
    return tple[:index] + (tple[index] % mod,) + tple[index:]

from tuple_manipulation import *
import matplotlib.pyplot as plt


class ColoredGrid:
    """
    >>> g = ColoredGrid(d, n, k, m)
    creates a grid 'g' composed of all points in '[0, n-1]^d'.
    They can be colored using the 'k' colors '{0, 1, ..., k-1}'.
    The block size is 'm'.
    """

    def __init__(self, d, n, k, m):
        self.d = d
        self.n = n
        self.k = k
        self.m = m
        self.pixel_to_color = {pixel: None for pixel in iter_pixels(self.d, self.n)}

    def color_pixel(self, pixel, color):
        self.pixel_to_color[pixel] = color

    def is_properly_colored(self):
        color_vectors = set()
        for anchor in iter_pixels(self.d, self.n - self.m + 1):
            color_vector = self.color_vector_from_block(anchor)
            if color_vector in color_vectors:
                return False
            color_vectors.add(color_vector)
        return True

    def is_pixel_in(self, pixel):
        return pixel in self.pixel_to_color

    def color_vector_from_block(self, anchor):
        if len(anchor) != self.d:
            raise ValueError
        for x in anchor:
            if x < 0 or x > self.n - self.m:
                raise ValueError
        color_vector = [0] * self.k
        for pixel in self.iter_block_pixels(anchor):
            color_vector[self.pixel_to_color[pixel]] += 1
        return tuple(color_vector)

    def iter_block_pixels(self, anchor):
        for pixel in iter_pixels(self.d, self.m):
            yield add_pixels(anchor, pixel)

    def color_periodically(self, period):
        for pixel in self.pixel_to_color:
            self.color_pixel(pixel, sum(pixel) % period)

    def copy(self):
        grid = ColoredGrid(self.d, self.n, self.k, self.m)
        grid.pixel_to_color = self.pixel_to_color.copy()
        return grid

    def plot(self):
        if self.d != 2:
            raise ValueError
        data = [[0 for _ in range(self.n)] for _ in range(self.n)]
        for pixel, color in self.pixel_to_color.items():
            data[pixel[0]][pixel[1]] = color / (self.k - 1)
        plt.figure()
        plt.imshow(data)

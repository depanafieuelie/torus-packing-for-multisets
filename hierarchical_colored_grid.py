from colored_grid import ColoredGrid
from tuple_manipulation import *
import profiles
import localization


class HierarchicalColoredGrid(ColoredGrid):
    """
    'HierarchicalColoredGrid(d, b, t)' creates a properly colored grid
    using the algorithm we presented in STACS24,
    with parameters 'd' (dimension), 'b' and 't'.
    The block size 'm' and the grid size 'n' are computable from 'd', 'b' and 't'
    (see the overleaf draft for the formulae).

    Example. We import the main package

    >>> from hierarchical_colored_grid import HierarchicalColoredGrid

    and fix the dimension d and parameters b and t

    >>> d, b, t = 2, 2, 1

    We generate a coloured grid with those parameters

    >>> g = HierarchicalColoredGrid(d, b, t)

    and test whether this grid is indeed properly colored.

    >>> g.is_properly_colored()

    Now consider an arbitrary point of the grid.

    >>> point = (5, 7)

    The window of corner 'point' contains colors.
    Their multiset is represented as a tuple
    counting the occurrence of each color.

    >>> color_vector = g.color_vector_from_block(point)

    By design, this multiset uniquely determine
    the location of the window.

    >>> g.locate(color_vector)

    Furthermore, the computation of the location from the multiset
    is achieved in constant complexity, for b and d fixed.
    """

    def __init__(self, d, b, t, make=True):
        self.k = b * d + 1
        self.m = 2 * b * d * t
        self.b = b
        self.s = self.m**(d - 1) // (2 * b * d)
        seq_t = [0, 2 * self.s]
        for _ in range(b):
            seq_t.append((2 * self.m * self.s + 1) * seq_t[-1] - 2)
        self.n = self.m * seq_t[b]
        super().__init__(d, self.n, self.k, self.m)
        self.null_color = self.k - 1
        self.dimension_to_colors = tuple(tuple(range(i * b, (i + 1) * b))
                                         for i in range(self.d))
        self.color_to_dimension = tuple(j // self.b for j in range(self.k - 1))
        if make:
            self.make()

    def make(self):
        self.color_periodically(self.k - 1)
        self.color_all_axis_slices()
        self.extend_axis_pattern()

    def locate(self, color_vector):
        """
        Input a multiset of colors, represented by a tuple marking the number of occurrences of each color.
        For example, the multiset {2, 2, 3, 1, 2, 1} is represented by the tuple (0, 2, 3, 1).
        Output the coordinate of the upper-left corner of the window of size m
        containing this multiset of colors.
        """
        return localization.locate(self.s, self.m, self.b, self.d, color_vector)

    def color_all_axis_slices(self):
        p = profiles.hierarchy(self.m, self.s, self.b)
        for i in range(self.d):
            for h in range(self.b):
                color = self.dimension_to_colors[i][h]
                profile = p[h]
                self.color_axis_from_profile(i, color, profile)

    def extend_axis_pattern(self):
        for pixel, color in self.pixel_to_color.items():
            if color == self.null_color:
                continue
            i = self.color_to_dimension[color]
            reference_pixel = [pixel[j] % self.m for j in range(self.d)]
            reference_pixel[i] = pixel[i]
            reference_pixel = tuple(reference_pixel)
            if self.pixel_to_color[reference_pixel] == self.null_color:
                self.erase_pixel(pixel)

    def erase_pixel(self, pixel):
        self.color_pixel(pixel, self.null_color)

    def color_axis_from_profile(self, i, color, profile):
        anchor_list = [0 for _ in range(self.d)]
        for z in range(self.n):
            number_to_erase = self.m**(self.d - 1) // (self.k - 1) - profile[z]
            anchor_list[i] = z
            anchor = tuple(anchor_list)
            self.partially_erase_slice(i, anchor, color, number_to_erase)

    def partially_erase_slice(self, i, anchor, color, number_to_erase):
        for pixel in self.iter_slice(i, anchor):
            if number_to_erase == 0:
                break
            if self.pixel_to_color[pixel] == color:
                self.erase_pixel(pixel)
                number_to_erase -= 1

    def iter_slice(self, i, anchor):
        for pixel in iter_pixels(self.d - 1, self.m):
            point = insert_in_tuple(pixel, i, 0)
            point = add_pixels(point, anchor)
            point = tuple(x % self.n for x in point)
            yield point

import unittest
from tuple_manipulation import *


class TestTupleManipulation(unittest.TestCase):

    def test_iter_pixels(self):
        d, n = 2, 3
        computed = set(iter_pixels(d, n))
        expected = {(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)}
        self.assertEqual(computed, expected)

    def test_add_pixels(self):
        p0 = (1, 2, 3)
        p1 = (-1, 0, 2)
        expected = (0, 2, 5)
        computed = add_pixels(p0, p1)
        self.assertEqual(expected, computed)

    def test_insert_in_tuple(self):
        tple = (3, 1, 4, 2)
        index = 2
        value = 12
        expected = (3, 1, 12, 4, 2)
        computed = insert_in_tuple(tple, index, value)
        self.assertEqual(expected, computed)
        tple = (3, 1, 4, 2)
        index = 0
        value = 12
        expected = (12, 3, 1, 4, 2)
        computed = insert_in_tuple(tple, index, value)
        self.assertEqual(expected, computed)

    def test_multiply_tuple(self):
        tple = (3, 1, 4)
        scalar = 2
        expected = (6, 2, 8)
        computed = multiply_tuple(tple, scalar)
        self.assertEqual(expected, computed)


if __name__ == '__main__':
    unittest.main()

import unittest
from profiles import *
from localization import *


class Localization(unittest.TestCase):

    def test_decode_profile(self):
        for s in range(1, 5):
            for m in range(2, 5):
                for tt in range(5):
                    dualp = dual_profile(s, m, tt)
                    self.assertEqual(0, decode_profile(s, m, tt, dualp[0]))
                    for i in range(1, len(dualp)):
                        d = decode_profile(s, m, tt, dualp[i])
                        self.assertEqual(dualp[d], dualp[i])
                        self.assertNotEqual(dualp[d-1], dualp[i])

    def test_decode_vector_sum_packing(self):
        for s in range(1, 4):
            for m in range(2, 5):
                for b in range(1, 4):
                    vector = dual_vector_sum_packing(s, m, b)
                    for i in range(len(vector)):
                        self.assertEqual(i, decode_vector_sum_packing(s, m, b, vector[i]))


if __name__ == '__main__':
    unittest.main()

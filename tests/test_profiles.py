import unittest
from profiles import *


class TestProfiles(unittest.TestCase):

    def test_hierarchy(self):
        for m in range(2, 5):
            for s in range(1, 5):
                for b in range(1, 4):
                    h = hierarchy(m, s, b)
                    for hh in h:
                        self.assertEqual(len(hh), len(h[0])), f"Wrong lengths for (m,s,b) = {m,s,b}"
                    d = dual_from_hierarchy(h, m)
                    z = set(zip(*d))
                    self.assertEqual(len(z), len(h[0])), f"hierarchy failed on (m,s,b) = {m, s, b}"

    def test_profile(self):
        for s in range(1, 5):
            for m in range(2, 5):
                for tt in range(4):
                    sequence = profile(s, m, tt)
                    expected = dual_from_seq(sequence, m)
                    computed = dual_profile(s, m, tt)
                    self.assertEqual(expected, computed), f"wrong sequence for (s,m,t) = {s, m, tt}"
                    expected_length = 0
                    if tt == 0:
                        expected_length = 2 * m * s
                    else:
                        expected_length = m * ((2 * m * s + 1) * tt - 2)
                    self.assertEqual(len(sequence), expected_length), f"wrong length for (s,m,t) = {s, m, tt}"

    def test_profile_0(self):
        for s in range(1, 5):
            for m in range(2, 5):
                sequence = profile_0(s, m)
                expected = dual_from_seq(sequence, m)
                computed = dual_profile_0(s, m)
                self.assertEqual(expected, computed), f"wrong sequence for (s,m) = {s, m}"
                self.assertEqual(len(sequence), 2 * m * s), f"wrong length for (s,m) = {s, m}"

import unittest
from hierarchical_colored_grid import *


class TestHierarchicalColoredGrid(unittest.TestCase):

    def test_dimension_and_colors(self):
        d, b, t = 2, 2, 1
        grid = HierarchicalColoredGrid(d, b, t, make=False)
        for color in range(grid.k - 1):
            i = grid.color_to_dimension[color]
            self.assertTrue(color in grid.dimension_to_colors[i])
        for colors in grid.dimension_to_colors:
            self.assertEqual(len(colors), grid.b)

    def test(self):
        arguments = ((2, 1, 1), (2, 2, 1), (2, 1, 2))
        for d, b, t in arguments:
            grid = HierarchicalColoredGrid(d, b, t)
            self.assertTrue(grid.is_properly_colored())

    def test_locate(self):
        d, b, t = 2, 2, 1
        grid = HierarchicalColoredGrid(d, b, t)
        for row in range(grid.n - grid.m):
            for column in range(grid.n - grid.m):
                point = (row, column)
                color_vector = grid.color_vector_from_block(point)
                self.assertEqual(grid.locate(color_vector), point)


if __name__ == '__main__':
    unittest.main()

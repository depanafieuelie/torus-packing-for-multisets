import unittest
from colored_grid import *


def get_grid_example_0():
    d, n, k, m = 2, 3, 3, 2
    grid = ColoredGrid(d, n, k, m)
    grid.color_pixel((0, 0), 1)
    grid.color_pixel((0, 1), 0)
    grid.color_pixel((0, 2), 2)
    grid.color_pixel((1, 0), 1)
    grid.color_pixel((1, 1), 2)
    grid.color_pixel((1, 2), 1)
    grid.color_pixel((2, 0), 1)
    grid.color_pixel((2, 1), 0)
    grid.color_pixel((2, 2), 1)
    return grid


def get_grid_example_1():
    d, n, k, m = 2, 3, 3, 2
    grid = ColoredGrid(d, n, k, m)
    grid.color_pixel((0, 0), 1)
    grid.color_pixel((0, 1), 1)
    grid.color_pixel((0, 2), 2)
    grid.color_pixel((1, 0), 1)
    grid.color_pixel((1, 1), 2)
    grid.color_pixel((1, 2), 1)
    grid.color_pixel((2, 0), 2)
    grid.color_pixel((2, 1), 0)
    grid.color_pixel((2, 2), 1)
    return grid


class TestColoredGrid(unittest.TestCase):

    def test_color_vector_from_block(self):
        grid = get_grid_example_0()
        self.assertEqual(grid.color_vector_from_block((0, 1)), (1, 1, 2))
        self.assertEqual(grid.color_vector_from_block((1, 0)), (1, 2, 1))

    def test_is_properly_colored(self):
        grid = get_grid_example_0()
        self.assertFalse(grid.is_properly_colored())
        grid = get_grid_example_1()
        self.assertTrue(grid.is_properly_colored())

    def test_iter_block_pixels(self):
        anchor = (2, 3, 1)
        m = 2
        d = len(anchor)
        n = 4 * m
        k = 2
        grid = ColoredGrid(d, n, k, m)
        computed = set(grid.iter_block_pixels(anchor))
        expected = {(2, 3, 1), (3, 3, 1), (2, 4, 1), (3, 4, 1), (2, 3, 2), (3, 3, 2), (2, 4, 2), (3, 4, 2)}
        self.assertEqual(computed, expected)

    def test_color_periodically(self):
        d, n, k, m = 2, 6, 5, 3
        grid = ColoredGrid(d, n, k, m)
        period = 3
        grid.color_periodically(period)
        for pixel, color in grid.pixel_to_color.items():
            self.assertEqual(color, sum(pixel) % period)


if __name__ == '__main__':
    unittest.main()

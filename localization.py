
def locate(s, m, b, d, color_vector):
    res = [0 for _ in range(d)]
    for dimension in range(d):
        vector = vector_from_color_vector_and_dimension(b, color_vector, dimension)
        res[dimension] = decode_vector_sum_packing(s, m, b, vector)
    return tuple(res)


def decode_vector_sum_packing(s, m, b, v, is_short=False):
    assert len(v) == b
    if b == 1:
        j = decode_profile(s, m, 0, v[0])
        if is_short and j != 0:
            j -= 1
        return j
    tt = 2 * s
    for _ in range(b - 2):
        tt = (2 * m * s + 1) * tt - 2
    j = decode_profile(s, m, tt, v[-1])
    if is_short and j != 0:
        j -= 1
    if ((is_short or v[b - 1] > 0)
            and ((v[b - 1] % 2 == 0 and (v[b - 1] // 2) % s == 0)
                 or (v[b - 1] % 2 == 1 and ((v[b - 1] - 1) // 2) % s == 0))):
        j += decode_vector_sum_packing(s, m, b - 1, v[:b - 1], is_short=True)
    else:
        j += decode_vector_sum_packing(s, m, b - 1, v[:b - 1])
    return j


def decode_profile(s, m, tt, v):
    if tt == 0:
        if v % 2 == 0:
            return v // 2
        else:
            return 2 * m * s - 1 - (v - 1) // 2
    else:
        if v % 2 == 0:
            row = v // 2 // s
            column = v // 2 % s
            return (int(row > 0) * (m * tt * s * row - (row - 1))
                    + int(column > 0) * (column * m * tt - 1 + int(row == 0)))
        else:
            row = (2 * m * s - v + 1) // 2 // s
            column = (2 * m * s - v + 1) // 2 % s
            return (1 + m * (m * tt * s - 1)
                    + int(row > 0) * (m * tt * s * row - row)
                    + int(column > 0) * (column * m * tt - 1))


def vector_from_color_vector_and_dimension(b, color_vector, dimension):
    return color_vector[dimension * b: (dimension + 1) * b]

def grid_localize():
    pass

from tuple_manipulation import add_pixels


def vector_sum_packing(s, m, b):
    return list(zip(*hierarchy(m, s, b)))


def dual_vector_sum_packing(s, m, b):
    v = vector_sum_packing(s, m, b)
    vv = v + v[:m]
    res = [(0,) * b for _ in range(len(v))]
    for i in range(len(v)):
        for j in range(i, i + m):
            res[i] = add_pixels(res[i], vv[j])
    return res


def hierarchy(m, s, b):
    if b < 1:
        raise ValueError
    if b == 1:
        return (profile_0(s, m),)

    def expand(tple):
        short_tple = tple[1:]
        line = tple * (s - 1) + short_tple
        return tple + line * (2 * m)

    tt = 2*s
    for _ in range(b-2):
        tt = (2*m*s + 1)*tt - 2
    p = profile(s, m, tt)
    h = hierarchy(m, s, b-1)
    return tuple(expand(hh) for hh in h) + (p,)


def dual_from_hierarchy(h, m):
    return tuple(dual_from_seq(hh, m) for hh in h)


def dual_from_seq(seq, m):
    return tuple(sum(seq[(i + j) % len(seq)] for j in range(m)) for i in range(len(seq)))


def profile(s, m, tt):
    if tt == 0:
        return profile_0(s, m)
    res = tuple()
    for i in range(m):
        for j in range(s):
            cell = (0,) * (m - i - 1) + (2*j,) + (2*s,) * i
            res += cell * tt
    i, j = m, 0
    cell = (2*s,) * (m*tt - 1)
    res += cell
    i = m
    for j in range(1, s):
        cell = (2*s - 2*j + 1,) + (2*s,) * (m-1)
        res += cell * tt
    i, j = m+1, 0
    cell = (1,) + (2*s,) * (m-1)
    res += cell * (tt-1)
    i = m+1
    for j in range(1, s):
        cell = (1,) + (2*s,) * (m-2) + (2*s - 2*j,)
        res += cell * tt
    for i in range(m+2, 2*m):
        for j in range(s):
            cell = (1,) + (2*s,) * (2*m - i - 1) + (2*s - 2*j,) + (0,) * (i - m - 1)
            res += cell * tt
    i, j = 2*m, 0
    cell = (1,) + (0,) * (m-1)
    res += cell * (tt - 1) + (1,)
    return res


def dual_profile(s, m, tt):
    if tt == 0:
        return dual_profile_0(s, m)
    res = (0,)
    for i in range(m):
        j = 0
        res += (2 * i * s,) * (m * tt - 1)
        for j in range(1, s):
            res += (2 * i * s + 2 * j,) * (m*tt)
    i, j = m, 0
    res += (2 * m * s,) * (m * tt - 1)
    i = m
    for j in range(1, s):
        res += (2 * m * s + 1 - 2 * j,) * (m*tt)
    for i in range(m+1, 2*m):
        j = 0
        res += (2 * (2 * m - i) * s + 1,) * (m * tt - 1)
        for j in range(1, s):
            res += (2 * (2 * m - i) * s + 1 - 2 * j,) * (m*tt)
    i, j = 2*m, 0
    res += (1,) * (m * tt - 1)
    return res


def profile_0(s, m):
    res = tuple()
    for c in range(s):
        cell = (2*c,) * m
        res += cell
    for c in range(s):
        cell = (2*(s - c),) * (m-1) + (2*(s - c) - 1,)
        res += cell
    return res


def dual_profile_0(s, m):
    res = tuple(2*c for c in range(m*s))
    res += tuple(2*(m*s - c) - 1 for c in range(m*s))
    return res

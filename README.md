## Torus packing for multisets


## Description

This project contains Python3 code that illustrates the algorithms from the paper
"Robot positioning using torus packing for multisets" by (in alphabetical order)
- Chung Shue Chen (Nokia Bell Labs, France)
- Élie de Panafieu (Nokia Bell Labs, France)
- Peter Keevash, (Mathematical Institute, University of Oxford, UK)
- Sean Kennedy (Nokia Bell Labs, Canada)
- Adrian Vetta (McGill University, Canada)
presented at the International Colloquium on Automata, Languages and Programming, ICALP Track A 2024.

In the terminology of "Universal cycles for combinatorial structures"
(Fan Chung, Persi Diaconis, Ron Graham, 1992) and
"Universal Cycle Packings and Coverings for k-Subsets of an n-Set"
(Michał Dȩbski, Zbigniew Lonc, 2016), our algorithm outputs
a universal cycle packing of dimension d for multisets.

It inputs integer parameters 'd', 'b' and 't'
and outputs a colored grid.
Let k = d*b + 1, m = 2*b*d*t, s = (2*b*d)**(d-2) * t**(d-1) and
n = (2*m*s + 1)**(b-1) * (2*m*s - 1/s) + 1/s.
The grid has dimension 'd' and size 'n'.
Each pixel receives a color in {0, 1, ..., k-1}.
The grid is considered as a torus.
The main property is that no two d-dimensional subsquare of size m
correspond to the same multiset of colors.


## Dependencies

matplotlib 3.6.2


## Example

We import the main package

    >>> from hierarchical_colored_grid import HierarchicalColoredGrid

and fix the dimension d and parameters b and t

    >>> d, b, t = 2, 2, 1

We generate a coloured grid with those parameters

    >>> g = HierarchicalColoredGrid(d, b, t)

and test whether this grid is indeed properly colored.

    >>> g.is_properly_colored()

We can also visualize the grid.

    >>> g.plot()

Now consider an arbitrary point of the grid.

    >>> point = (5, 7)

The window of corner 'point' contains colors.
Their multiset is represented as a tuple
counting the occurrence of each color.

    >>> color_vector = g.color_vector_from_block(point)

By design, this multiset uniquely determine
the location of the window.

    >>> g.locate(color_vector)

Furthermore, the computation of the location from the multiset
is achieved in constant complexity, for b and d fixed.


## Files

'hierarchical_colored_grid.py' contains the main class 'HierarchicalColoredGrid'
implementing our algorithm.

'colored_grid.py' defines the base class 'ColoredGrid'.

'profiles.py', 'localization' and 'tuple_manipulation.py' contain auxiliary functions.

The 'tests' directory contains automatic tests for the whole project.

## Authors and acknowledgment

The Python code has been written by Élie de Panafieu, based on the algorithms from the paper "Robot positioning using torus packing for multisets" cited at the beginning of this document.

## License

MIT License

Copyright (c) [2024] [Élie de Panafieu]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

